# neufbox-status

![screenshot](https://i.imgur.com/kwtYoHM.png)

## Description

Permet de regrouper toutes les informations de la box SFR / RED / Neuf en une seule page. Utilise [neufbox-api-client](https://gitlab.com/liamjack/neufbox-api-client) pour recuperer les informations à travers l'API de la box.
