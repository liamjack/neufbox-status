<?php

require_once "vendor/autoload.php";

$neufboxApiClient = new LiamJack\NeufboxApiClient();

$neufboxApiClient->getAuthToken();
$neufboxApiClient->checkAuthToken("admin", "admin");

$system = $neufboxApiClient->getSystemInfo();
$dsl = $neufboxApiClient->getDslInfo();
$lanHosts = $neufboxApiClient->getLanHostList();

?>
<!doctype html>
<html>
    <head>
        <title>neufbox-status</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h1>neufbox-status</h1>

            <hr>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <h3 class="card-header">System</h3>
                        <div class="card-body">
                            <strong>MAC Address</strong>: <code><?php echo $system['mac_addr']; ?></code><br>
                            <strong>Main firmware version</strong>: <code><?php echo $system['version_mainfirmware']; ?></code><br>
                            <strong>Rescue firmware version</strong>: <code><?php echo $system['version_rescuefirmware']; ?></code><br>
                            <strong>Bootloader version</strong>: <code><?php echo $system['version_bootloader']; ?></code><br>
                            <strong>DSL Driver version</strong>: <code><?php echo $system['version_dsldriver']; ?></code><br><br>

                            <strong>Uptime</strong>: <?php echo gmdate("d", $system['uptime']); ?> day(s) <?php echo gmdate("H", $system['uptime']); ?> hour(s) <?php echo gmdate("i", $system['uptime']); ?> minute(s)<br>
                            <strong>Input voltage</strong>: <?php echo $system['alimvoltage'] / 1000; ?> V<br>
                            <strong>CPU Temperature</strong>: <?php echo $system['temperature'] / 1000; ?> °C<br>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card">
                        <h3 class="card-header">DSL</h3>
                        <div class="card-body">
                            <strong>Status</strong>: <?php echo $dsl['status']; ?><br>
                            <strong>Mode</strong>: <?php echo $dsl['linemode']; ?><br>
                            <strong>Uptime</strong>: <?php echo gmdate("d", $dsl['uptime']); ?> day(s) <?php echo gmdate("H", $dsl['uptime']); ?> hour(s) <?php echo gmdate("i", $dsl['uptime']); ?> minute(s)<br>
                            <strong>Downlink rate</strong>: <?php echo $dsl['rate_down']; ?> Kbps<br>
                            <strong>Uplink rate</strong>: <?php echo $dsl['rate_up']; ?> Kbps<br>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 mt-5">
                    <div class="card">
                        <h3 class="card-header">LAN</h3>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>MAC</th>
                                <th>IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($lanHosts as $lanHost) { ?>
                                <tr>
                                    <td><?php echo $lanHost['name']; ?></td>
                                    <td><?php echo $lanHost['mac']; ?></td>
                                    <td><?php echo $lanHost['ip']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
